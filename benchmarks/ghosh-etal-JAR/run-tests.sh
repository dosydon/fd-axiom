
FD=../../src/search/downward-release
ulimit -t 3600 -m 3000000 -v 3000000

## run blind and 3 variants of h^max on blocker problems

run_all_configs() {
    pfile=$1
    pname=`dirname $pfile | sed -e 'y+/+_+'`_`basename $pfile .pre`
    # echo $pfile : $pname
    ${FD} --search 'astar(blind())' --internal-plan-file ${pname}.blind.soln < $pfile >& ${pname}.blind.log
    ${FD} --search 'astar(hmax())' --internal-plan-file ${pname}.naive_hmax.soln < $pfile >& ${pname}.naive_hmax.log
    ${FD} --search 'astar(hmax_with_axioms(tvr=true))' --internal-plan-file ${pname}.tv_hmax.soln < $pfile >& ${pname}.tv_hmax.log
    ${FD} --search 'astar(hmax_with_axioms(tvr=false))' --internal-plan-file ${pname}.asp_hmax.soln < $pfile >& ${pname}.asp_hmax.log
}

run_strips_configs() {
    pfile=$1
    pname=`dirname $pfile | sed -e 'y+/+_+'`_`basename $pfile .pre`
    # echo $pfile : $pname
    ${FD} --search 'astar(blind())' --internal-plan-file ${pname}.blind.soln < $pfile >& ${pname}.blind.log
    ${FD} --search 'astar(hmax())' --internal-plan-file ${pname}.naive_hmax.soln < $pfile >& ${pname}.naive_hmax.log
}

run_all_configs DOOREXAMPLE/domain-fixed/cc2/problem2.cc2.pre
run_all_configs DOOREXAMPLE/domain-fixed/cc2/problem.cc2.pre
run_all_configs DOOREXAMPLE/domain-broken/cc2/problem2.cc2.pre
run_all_configs DOOREXAMPLE/domain-broken/cc2/problem.cc2.pre

run_all_configs ACC/cc2/acc.badgoal2.cc2.pre
run_all_configs ACC/cc2/acc.badgoal5.cc2.pre
run_all_configs ACC/cc2/acc.goodgoal7.cc2.pre
run_all_configs ACC/cc2/acc.badgoal4.cc2.pre
run_all_configs ACC/cc2/acc.badgoal3.cc2.pre
run_all_configs ACC/cc2/acc.goodgoal6.cc2.pre
run_all_configs ACC/cc2/acc.badgoal1.cc2.pre
run_all_configs ACC/cc2/acc.goodgoal8.cc2.pre

run_strips_configs DOOREXAMPLE/domain-fixed/cc1/problem2.cc1.pre
run_strips_configs DOOREXAMPLE/domain-fixed/cc1/problem.cc1.pre
run_strips_configs DOOREXAMPLE/domain-broken/cc1/problem2.cc1.pre
run_strips_configs DOOREXAMPLE/domain-broken/cc1/problem.cc1.pre

run_strips_configs ACC/cc1/acc.goodgoal7.cc1.pre
run_strips_configs ACC/cc1/acc.badgoal5.cc1.pre
run_strips_configs ACC/cc1/acc.badgoal3.cc1.pre
run_strips_configs ACC/cc1/acc.goodgoal8.cc1.pre
run_strips_configs ACC/cc1/acc.badgoal4.cc1.pre
run_strips_configs ACC/cc1/acc.badgoal1.cc1.pre
run_strips_configs ACC/cc1/acc.goodgoal6.cc1.pre
run_strips_configs ACC/cc1/acc.badgoal2.cc1.pre
