(DEFINE (PROBLEM door-problem) 
    (:DOMAIN door-example)
  (:INIT     
               (stationary)
)
  (:GOAL (and 
               (high-speed)
               (not (doors-locked))
         )
  )
)
