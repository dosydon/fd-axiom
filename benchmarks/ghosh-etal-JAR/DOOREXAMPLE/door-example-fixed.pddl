(define (domain door-example)
(:requirements :strips)
(:predicates
(ARM-AUTOLOCK)
(ARM-AUTOUNLOCK)
(DOORS-CLOSED)
(DOORS-LOCKED)
(KEY-IGNITION)
(REMOTE-UNLOCK-CMD)
(ENGINE-ON)
(TRANS-MODE-DRIVE)
(HIGH-SPEED)
(LOW-SPEED)
(PREV-LOW-SPEED)
(STATIONARY))
(:action control-C1-ARM-AUTO-LOCK
:precondition ( and
(PREV-LOW-SPEED)
(not (LOW-SPEED))
(HIGH-SPEED))
:effect ( and (ARM-AUTOLOCK)
(not (PREV-LOW-SPEED))))
(:action control-MARK-PREV-LOW-SPEED
:precondition ( and
(LOW-SPEED)
(not (PREV-LOW-SPEED)))
:effect ( and (PREV-LOW-SPEED)))
(:action control-UNMARK-PREV-LOW-SPEED
:precondition ( and
(PREV-LOW-SPEED)
(not (LOW-SPEED))
(not (HIGH-SPEED)))
:effect ( and (not (PREV-LOW-SPEED))))
(:action control-C2-AUTO-LOCK
:precondition ( and
(ARM-AUTOLOCK)
(DOORS-CLOSED)
(not (DOORS-LOCKED)))
:effect ( and (DOORS-LOCKED)
(not (ARM-AUTOLOCK))))
(:action control-C3-ARM-AUTO-UNLOCK
:precondition ( and
(REMOTE-UNLOCK-CMD)
(DOORS-LOCKED)
(not (HIGH-SPEED))
;; to fix, uncomment above.
(not (ARM-AUTOUNLOCK)))
:effect ( and (ARM-AUTOUNLOCK)))

(:action control-C4-AUTO-UNLOCK
:precondition ( and
(ARM-AUTOUNLOCK)
(DOORS-LOCKED))
:effect ( and (not (DOORS-LOCKED))
(not (ARM-AUTOUNLOCK))))
(:action E1-OPEN-DOORS
:precondition ( and
(not (DOORS-LOCKED))
(not (TRANS-MODE-DRIVE))
(STATIONARY)
(DOORS-CLOSED))
:effect ( and (not (DOORS-CLOSED))))
(:action E2-CLOSE-DOORS
:precondition ( and
(not (DOORS-CLOSED)))
:effect ( and (DOORS-CLOSED)))
(:action E3-PUT-KEY-IN-IGNITION
:precondition ( and
(not (KEY-IGNITION)))
:effect ( and (KEY-IGNITION)))
(:action E4-RUN-ENGINE
:precondition ( and
(KEY-IGNITION)
(not (ENGINE-ON)))
:effect ( and (ENGINE-ON)))
(:action E5-PUT-TRANSMISSION-DRIVE
:precondition ( and
(ENGINE-ON)
(DOORS-CLOSED)
(not (TRANS-MODE-DRIVE)))
:effect ( and (TRANS-MODE-DRIVE)))
(:action E6-SPEED-STAT-TO-LOW
:precondition ( and
(ENGINE-ON)
(TRANS-MODE-DRIVE)
(STATIONARY))
:effect ( and (not (STATIONARY))
(LOW-SPEED)))
(:action E6-SPEED-LOW-TO-HIGH
:precondition ( and
(ENGINE-ON)
(TRANS-MODE-DRIVE)
(LOW-SPEED))
:effect ( and (not (LOW-SPEED))
(HIGH-SPEED)))
(:action E7-COMMAND-REMOTE-UNLOCK
:precondition ( and
(DOORS-LOCKED)
(not (REMOTE-UNLOCK-CMD)))
:effect ( and (REMOTE-UNLOCK-CMD)))
)

