#!/usr/bin/python

from __future__ import print_function
import sys

varnum = 0
stonevars = dict()

prefile = open(sys.argv[1])

for line in prefile:
    line = line.strip()
    if line == 'end_variable':
        varnum += 1
    elif line[0:8] == 'AT_STONE':
        sname = line[3:11]
        if sname not in stonevars:
            stonevars[sname] = set()
        stonevars[sname].add(varnum)
    # elif line[0:13] == 'Atom at(stone':
    #     sname = line[8:16]
    #     if sname not in stonevars:
    #         stonevars[sname] = set()
    #     stonevars[sname].add(varnum)
    # elif line[0:18] == 'Atom at-goal(stone':
    #     sname = line[13:21]
    #     if sname not in stonevars:
    #         stonevars[sname] = set()
    #     stonevars[sname].add(varnum)

prefile.close()

# for sname in stonevars:
#     print(sname + ": [", end='')
#     first = True
#     for vnum in stonevars[sname]:
#         if not first:
#             print(",", end='')
#         print(vnum, end='')
#         first = False
#     print("]")

print("patterns=[", end='')
firstpattern = True
for sname in stonevars:
    if not firstpattern:
        print(",", end='')
    print("[", end='')
    first = True
    for vnum in stonevars[sname]:
        if not first:
            print(",", end='')
        print(vnum, end='')
        first = False
    print("]", end='')
    firstpattern = False
print("]")
