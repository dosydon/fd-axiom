
(define (domain sokoban-sequential)
  (:requirements :typing :derived-predicates)
  (:types thing location direction - object
          player stone - thing)

  (:predicates
   (clear ?l - location)
   (blocked ?l - location)
   (at-goal ?s - stone)
   (IS-GOAL ?l - location)
   (IS-NONGOAL ?l - location)
   (can-reach ?p - player ?l - location)
   )

  (:functions
   (at ?t - thing) - location
   ;; (move-dir ?loc ?dir) -> location one step in ?dir from ?loc.
   (MOVE-DIR ?from - location ?dir - direction) - location
   ;; (move-dir-inverse ?loc ?dir) -> the ?from such that
   ;;   (move-dir ?from ?dir) = ?loc.
   (MOVE-DIR-INVERSE ?to - location ?dir - direction) - location
   (total-cost) - number
   )

  ;; axiom for at-goal
  (:derived (at-goal ?s - stone)
	    (exists (?l - location) (and (is-goal ?l) (= (at ?s) ?l))))

  ;; axiom for clear. note that only stones count as obstacles,
  ;; the player does not; he will always move so that he is not
  ;; in the way of the square he's pushing into.
  (:derived (blocked ?l - location)
	    (exists (?s - stone) (= (at ?s) ?l)))
  (:derived (clear ?l - location) (not (blocked ?l)))

  ;; axioms for can-reach
  (:derived (can-reach ?p - player ?l - location)
	    (= (at ?p) ?l))

  (:derived (can-reach ?p - player ?l - location)
	    (and (clear ?l)
		 (exists (?d - direction)
			 (can-reach ?p (move-dir-inverse ?l ?d)))))


  (:action push
   :parameters (?p - player ?s - stone ?from - location ?dir - direction)
   :precondition (and
		  (= (at ?s) ?from)
		  (can-reach ?p (move-dir-inverse ?from ?dir))
		  (clear (move-dir ?from ?dir))
		  )
   :effect       (and
		  (assign (at ?p) ?from)
		  (assign (at ?s) (move-dir ?from ?dir))
		  (increase (total-cost) 1)
		  )
   )

  )
