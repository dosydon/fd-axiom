FD=../../src/search/downward-release
ulimit -t 3600 -m 3000000 -v 3000000

${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[119],[120]]))' --internal-plan-file p01.opt08.fstrips.cpdb.soln < p01.opt08.fstrips.pre >& p01.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[57],[58]]))' --internal-plan-file p02.opt08.fstrips.cpdb.soln < p02.opt08.fstrips.pre >& p02.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[47],[48]]))' --internal-plan-file p03.opt08.fstrips.cpdb.soln < p03.opt08.fstrips.pre >& p03.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[105],[106],[107]]))' --internal-plan-file p04.opt08.fstrips.cpdb.soln < p04.opt08.fstrips.pre >& p04.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[72],[73],[74],[75],[76],[77],[78],[79]]))' --internal-plan-file p05.opt08.fstrips.cpdb.soln < p05.opt08.fstrips.pre >& p05.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[51],[52],[53]]))' --internal-plan-file p06.opt08.fstrips.cpdb.soln < p06.opt08.fstrips.pre >& p06.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[222],[223],[224]]))' --internal-plan-file p07.opt08.fstrips.cpdb.soln < p07.opt08.fstrips.pre >& p07.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[78],[79],[80],[81]]))' --internal-plan-file p08.opt08.fstrips.cpdb.soln < p08.opt08.fstrips.pre >& p08.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[92],[93],[94],[95]]))' --internal-plan-file p09.opt08.fstrips.cpdb.soln < p09.opt08.fstrips.pre >& p09.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[105],[106],[107],[108]]))' --internal-plan-file p10.opt08.fstrips.cpdb.soln < p10.opt08.fstrips.pre >& p10.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[142],[143],[144],[145]]))' --internal-plan-file p11.opt08.fstrips.cpdb.soln < p11.opt08.fstrips.pre >& p11.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[92],[93],[94],[95]]))' --internal-plan-file p12.opt08.fstrips.cpdb.soln < p12.opt08.fstrips.pre >& p12.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[98],[99],[100],[101]]))' --internal-plan-file p13.opt08.fstrips.cpdb.soln < p13.opt08.fstrips.pre >& p13.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[104],[105],[106]]))' --internal-plan-file p14.opt08.fstrips.cpdb.soln < p14.opt08.fstrips.pre >& p14.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[136],[137],[138],[139]]))' --internal-plan-file p15.opt08.fstrips.cpdb.soln < p15.opt08.fstrips.pre >& p15.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[219],[220],[221]]))' --internal-plan-file p16.opt08.fstrips.cpdb.soln < p16.opt08.fstrips.pre >& p16.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[215],[216],[217]]))' --internal-plan-file p17.opt08.fstrips.cpdb.soln < p17.opt08.fstrips.pre >& p17.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[108],[109],[110],[111]]))' --internal-plan-file p18.opt08.fstrips.cpdb.soln < p18.opt08.fstrips.pre >& p18.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[82],[83],[84],[85]]))' --internal-plan-file p19.opt08.fstrips.cpdb.soln < p19.opt08.fstrips.pre >& p19.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[693]]))' --internal-plan-file p20.opt08.fstrips.cpdb.soln < p20.opt08.fstrips.pre >& p20.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[81],[82],[72],[73],[74],[75],[76],[77],[78],[79],[80]]))' --internal-plan-file p21.opt08.fstrips.cpdb.soln < p21.opt08.fstrips.pre >& p21.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[108],[109],[110],[111]]))' --internal-plan-file p22.opt08.fstrips.cpdb.soln < p22.opt08.fstrips.pre >& p22.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[84],[85],[86],[87],[88]]))' --internal-plan-file p23.opt08.fstrips.cpdb.soln < p23.opt08.fstrips.pre >& p23.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[102],[103],[104],[105],[106]]))' --internal-plan-file p24.opt08.fstrips.cpdb.soln < p24.opt08.fstrips.pre >& p24.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[78],[79],[80],[81],[82]]))' --internal-plan-file p25.opt08.fstrips.cpdb.soln < p25.opt08.fstrips.pre >& p25.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[94],[95],[96],[97],[98]]))' --internal-plan-file p26.opt08.fstrips.cpdb.soln < p26.opt08.fstrips.pre >& p26.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[88],[89],[90],[91],[92],[93],[94]]))' --internal-plan-file p27.opt08.fstrips.cpdb.soln < p27.opt08.fstrips.pre >& p27.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[144],[145],[146],[147],[148]]))' --internal-plan-file p28.opt08.fstrips.cpdb.soln < p28.opt08.fstrips.pre >& p28.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[171],[169],[170],[160],[161],[162],[163],[164],[165],[166],[167],[168]]))' --internal-plan-file p29.opt08.fstrips.cpdb.soln < p29.opt08.fstrips.pre >& p29.opt08.fstrips.cpdb.log
${FD} --search 'astar(cpdbs(with_axioms=true, patterns=[[101],[99],[100],[90],[91],[92],[93],[94],[95],[96],[97],[98]]))' --internal-plan-file p30.opt08.fstrips.cpdb.soln < p30.opt08.fstrips.pre >& p30.opt08.fstrips.cpdb.log
