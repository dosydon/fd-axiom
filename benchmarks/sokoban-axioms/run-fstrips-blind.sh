
FD=../../src/search/downward-release

for prefile in p*.fstrips.pre; do
    pname=`basename $prefile .pre`
    (ulimit -t 3600 -m 300000 -v 3000000; $FD --search 'astar(blind())' --internal-plan-file ${pname}.blind.soln < $prefile >& ${pname}.blind.log)
done
