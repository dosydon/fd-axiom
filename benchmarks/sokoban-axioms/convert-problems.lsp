#!/usr/local/bin/ecl -shell

(setq *INVAL-DIR* (pathname-directory "~/pkg/inval/"))
(load (make-pathname :directory *INVAL-DIR* :name "inval" :type "lsp"))
(load (make-pathname :directory *INVAL-DIR* :name "rsk" :type "lsp"))

(defun basename-with-suffix (path suffix)
  (let ((bname (pathname-name path))
	(bext (pathname-type path)))
    (cond (bext (concatenate 'string bname "." suffix "." bext))
	  (t (concatenate 'string bname "." suffix)))))

(defun get-commandline-args ()
  (cdr ext:*unprocessed-ecl-command-args*))

(when (< (length (get-commandline-args)) 2)
  (format t "compiler <domain file> <problem file(s)>~%")
  (quit))

(setq dfile (first (get-commandline-args)))
(setq pfiles (rest (get-commandline-args)))

(setq *FSTRIPS* t)
(setq *suffix* (if *FSTRIPS* "opt08.fstrips" "opt08"))

(make-format-PDDL-friendly)
(dolist (pfile pfiles)
  (load-files dfile pfile)
  (format t "writing problem to ~a...~%" (basename-with-suffix pfile *suffix*))
  (with-open-file
   (stream (basename-with-suffix pfile *suffix*) :direction :output)
   (format stream "~&~w~%"
	   (make-problem-definition
	    ;; name domain-name objects
	    *problem-name* *domain-name* *objects*
	    ;; init
	    (append
	     (remove-if #'(lambda (atom)
			    (member (car atom) '(clear at-goal at move-dir)))
			*init*)
	     (if *FSTRIPS*
		 (append
		  (mapcar #'(lambda (atom)
			      (list '= (list 'at (second atom)) (third atom)))
			  (remove-if-not #'(lambda (atom) (eq (car atom) 'at))
					 *init*))
		  (mapcar #'(lambda (atom)
			      (list '= (list 'move-dir (second atom)
					     (fourth atom))
				    (third atom)))
			  (remove-if-not #'(lambda (atom)
					     (eq (car atom) 'move-dir))
					 *init*))
		  (mapcar #'(lambda (atom)
			      (list '= (list 'move-dir-inverse (third atom)
					     (fourth atom))
				    (second atom)))
			  (remove-if-not #'(lambda (atom)
					     (eq (car atom) 'move-dir))
					 *init*)))
	       nil))
	    ;; goal
	    *goal*
	    ;; metric-type metric-exp
	    *metric-type* *metric*)))
  )

(quit)
