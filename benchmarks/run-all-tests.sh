
(cd mincut; bash < run-tests.sh)
(cd misc-axioms-tests; bash < run-tests.sh)
(cd sokoban-axioms; bash < run-fstrips-cpdb.sh; bash < run-fstrips-blind.sh)
(cd sokoban-opt08-strips; bash < run-blind.sh; bash < run-cpdb.sh)
(cd ghosh-etal-JAR; bash < run-tests.sh)
(cd psr-middle; bash < run-tests.sh)
