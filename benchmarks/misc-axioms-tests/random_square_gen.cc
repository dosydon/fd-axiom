
#include <stdlib.h>
#include "graph.h"
#include "rng.h"

void make_random_graph
(HSPS::index_type d, HSPS::graph& g, HSPS::pair_set& e, HSPS::RNG& rng)
{
  HSPS::index_type s = (2*d) + 1;
  HSPS::index_type nn = s * s;
  g.init(nn);
  e.clear();

  for (HSPS::index_type i = 0; i < s; i++)
    for (HSPS::index_type j = 0; j + 1 < s; j++) {
      // horizontal
      g.add_undirected_edge((i*s) + j, (i*s) + j + 1);
      e.insert(HSPS::index_pair((i*s) + j, (i*s) + j + 1));
      // vertical
      g.add_undirected_edge((j*s) + i, ((j+1)*s) + i);
      e.insert(HSPS::index_pair((j*s) + i, ((j+1)*s) + i));
    }

  HSPS::index_type ne = (s - 1) * 2 * s;
  //std::cout << "ne = " << ne << ", |e| = " << e.size() << std::endl;
  assert(e.size() == ne);
  HSPS::index_type n_to_remove = ne / 4;
  // HSPS::index_type n_to_remove = (7*ne) / 24;
  HSPS::index_type tries = 4*ne;

  std::cerr << "removing " << n_to_remove << " edges..." << std::endl;
  while ((n_to_remove > 0) && (tries > 0)) {
    HSPS::index_type i = (HSPS::index_type)rng.random_in_range(e.size());
    assert(g.adjacent(e[i].first, e[i].second));
    g.remove_edge(e[i].first, e[i].second);
    g.remove_edge(e[i].second, e[i].first);
    if (g.connected()) {
      e.remove(i);
      n_to_remove -= 1;
      tries = 4*ne;
    }
    else {
      g.add_undirected_edge(e[i].first, e[i].second);
      tries -= 1;
    }
  }

  if (tries == 0) {
    std::cerr << "failed to remove edges!" << std::endl;
    exit(0);
  }
}

bool check_graph
(HSPS::index_type d, HSPS::graph& g)
{
  HSPS::index_type s = (2*d) + 1;
  HSPS::index_type nn = s * s;

  HSPS::index_type center = (s * d) + d;
  if (g.out_degree(center) < 2) {
    std::cerr << "start node is single-connected (1)" << std::endl;
    return false;
  }
  HSPS::index_set cn = g.bidirectional(center);
  for (HSPS::index_type i = 0; i < cn.size(); i++) {
    g.remove_edge(cn[i], center);
    g.remove_edge(center, cn[i]);
    if (!g.connected()) {
      std::cerr << "start node is single-connected (2)" << std::endl;
      return false;
    }
    g.add_undirected_edge(center, cn[i]);
  }

  HSPS::index_vec dte;
  g.distance(center, dte);
  HSPS::index_vec n_at_dte(0, nn);
  for (HSPS::index_type i = 0; i < s; i++) {
    HSPS::index_type n = i;
    assert(dte[n] != HSPS::no_such_index);
    assert(dte[n] < nn);
    n_at_dte[dte[n]] += 1;
    n = (s * (s - 1)) + i;
    assert(dte[n] != HSPS::no_such_index);
    assert(dte[n] < nn);
    n_at_dte[dte[n]] += 1;
    if ((i > 0) && (i + 1 < s)) {
      n = (s * i);
      assert(dte[n] != HSPS::no_such_index);
      assert(dte[n] < nn);
      n_at_dte[dte[n]] += 1;
      n = (s * i) + (s - 1);
      assert(dte[n] != HSPS::no_such_index);
      assert(dte[n] < nn);
      n_at_dte[dte[n]] += 1;
    }
  }

  HSPS::index_type n_total = 0;
  for (HSPS::index_type i = 0; i < nn; i++) {
    n_total += n_at_dte[i];

    if (n_at_dte[i] > 0)
      std::cerr << "at " << i << " = " << n_at_dte[i]
		<< ", <= " << i << " = " << n_total
		<< std::endl;

    if (n_total > (2 * i) - 1)
      return false;
  }

  return true;
}

void print_problem
(HSPS::index_type d, HSPS::pair_set& e, unsigned long id)
{
  std::cout << "(define (problem random-square-" << d
	    << "-" << id << ")" << std::endl;
  std::cout << " (:domain blocker-strips)" << std::endl;
  HSPS::index_type s = (2*d) + 1;
  HSPS::index_type nn = s * s;
  HSPS::index_type center = (s * d) + d;

  std::cout << " (:objects" << std::endl;
  for (HSPS::index_type i = 0; i < nn; i++)
    std::cout << "  n" << i;
  std::cout << ")" << std::endl;

  std::cout << " (:init" << std::endl;
  for (HSPS::index_type i = 0; i < e.size(); i++) {
    std::cout << "  (edge n" << e[i].first << " n" << e[i].second << ")"
	      << std::endl;
    std::cout << "  (edge n" << e[i].second << " n" << e[i].first << ")"
	      << std::endl;
  }
  for (HSPS::index_type i = 0; i < s; i++) {
    HSPS::index_type n = i;
    std::cout << "  (exit n" << n << ")" << std::endl;
    n = (s * (s - 1)) + i;
    std::cout << "  (exit n" << n << ")" << std::endl;
    if ((i > 0) && (i + 1 < s)) {
      n = (s * i);
      std::cout << "  (exit n" << n << ")" << std::endl;
      n = (s * i) + (s - 1);
      std::cout << "  (exit n" << n << ")" << std::endl;
    }
  }
  std::cout << "  (is-zero n0)" << std::endl;
  for (HSPS::index_type i = 0; i + 1 < nn; i++) {
    std::cout << "  (next n" << i << " n" << i + 1 << ")" << std::endl;
  }
  std::cout << "  (cat n" << center << ")" << std::endl;
  std::cout << "  (blockers-turn)" << std::endl;
  std::cout << "  )" << std::endl;

  std::cout << " (:goal (trapped))" << std::endl;
  std::cout << " )" << std::endl;
}

// void print_problem(HSPS::weighted_graph& g, int k, HSPS::RNG& rng)
// {
//   HSPS::pair_set cut;
//   g.set_edge_weight(1);
//   NTYPE f = g.min_cut(0, g.size() / 2, cut);
//   while (f == k) {
//     std::cout << ";; cut = " << cut << std::endl;
//     for (HSPS::index_type i = 0; i < cut.size(); i++)
//       g.increment_edge_weight(cut[i].first, cut[i].second, 1);
//     cut.clear();
//     f = g.min_cut(0, g.size() / 2, cut);
//   }
//   std::cout << "(define (problem min-cut-" << g.size()
// 	    << "-" << k << "-" << rng.seed_value() << ")" << std::endl;
//   std::cout << " (:domain min-cut)" << std::endl;
//   HSPS::index_set init_pos;
//   rng.select_fixed_set(init_pos, k, g.size() - 1);
//   assert(init_pos.size() == k);
//   HSPS::pair_set edges;
//   g.edges(edges);
//   std::cout << " (:objects" << std::endl;
//   for (HSPS::index_type i = 0; i < g.size(); i++)
//     std::cout << "  n" << i << " - node" << std::endl;
//   for (HSPS::index_type i = 0; i < edges.size(); i++)
//     std::cout << "  e-" << edges[i].first << "-" << edges[i].second
// 	      << " - edge" << std::endl;
//   for (HSPS::index_type i = 0; i < k; i++)
//     std::cout << "  b" << i << " - block" << std::endl;
//   std::cout << "  )" << std::endl;
//   std::cout << " (:init" << std::endl;
//   for (HSPS::index_type i = 0; i < edges.size(); i++) {
//     std::cout << "  (edge-from e-" << edges[i].first << "-" << edges[i].second
// 	      << " n" << edges[i].first << ")" << std::endl;
//     std::cout << "  (edge-to e-" << edges[i].first << "-" << edges[i].second
// 	      << " n" << edges[i].second << ")" << std::endl;
//     HSPS::index_type nt = edges[i].second;
//     for (HSPS::index_type j = 0; j < g.successors(nt).size(); j++)
//       std::cout << "  (adjacent e-" << edges[i].first
// 		<< "-" << edges[i].second << " e-" << nt
// 		<< "-" << g.successors(nt)[j] << ")" << std::endl;
//   }
//   for (HSPS::index_type i = 0; i < init_pos.size(); i++) {
//     HSPS::index_type nf = init_pos[i] + 1;
//     assert(!g.successors(nf).empty());
//     HSPS::index_type nt =
//       g.successors(nf)[rng.random_in_range(g.successors(nf).size())];
//     std::cout << "  (= (at b" << i << ") e-" << nf << "-" << nt << ")"
// 	      << std::endl;
//   }
//   std::cout << "  (source-node n0)" << std::endl;
//   std::cout << "  )" << std::endl;
//   std::cout << " (:goal (isolated n" << g.size() / 2 << "))"
// 	    << std::endl;
//   std::cout << " )" << std::endl;
// }

int main(int argc, char *argv[])
{
  if (argc < 2) {
    std::cout << argv[0] << ": <d>" << std::endl;
  }
  int d = atoi(argv[1]);
  assert(d > 0);
  HSPS::index_type s = (2*d) + 1;
  HSPS::index_type nn = s * s;

  HSPS::LC_RNG rng;
  rng.seed_with_pid();

  HSPS::graph g;
  HSPS::pair_set e;
  bool ok = false;
  do {
    make_random_graph(d, g, e, rng);
    ok = check_graph(d, g);
    if (!ok) std::cerr << "fail!" << std::endl;
  } while (!ok);

  print_problem(d, e, rng.random());
}
