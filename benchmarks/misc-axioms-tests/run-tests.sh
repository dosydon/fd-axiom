FD=../../src/search/downward-release
ulimit -t 3600 -m 3000000 -v 3000000

## run blind and 3 variants of h^max on blocker problems

run_all_configs() {
    pfile=$1
    pname=`basename $pfile .pre`
    ${FD} --search 'astar(blind())' --internal-plan-file ${pname}.blind.soln < $pfile >& ${pname}.blind.log
    ${FD} --search 'astar(hmax())' --internal-plan-file ${pname}.naive_hmax.soln < $pfile >& ${pname}.naive_hmax.log
    ${FD} --search 'astar(hmax_with_axioms(tvr=true))' --internal-plan-file ${pname}.tv_hmax.soln < $pfile >& ${pname}.tv_hmax.log
    ${FD} --search 'astar(hmax_with_axioms(tvr=false))' --internal-plan-file ${pname}.asp_hmax.soln < $pfile >& ${pname}.asp_hmax.log
}

run_all_configs blocker-strips-small.pre
run_all_configs blocker-strips-12.pre
run_all_configs bm2.pre
run_all_configs bm3.pre
run_all_configs bm4.pre
run_all_configs bm5.pre
# run_all_configs bm6.pre
