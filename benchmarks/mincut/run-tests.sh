FD=../../src/search/downward-release
ulimit -t 3600 -m 3000000 -v 3000000


run_all_configs() {
    pfile=$1
    pname=`basename $pfile .pre`
    #${FD} --search 'astar(blind())' --internal-plan-file ${pname}.blind.soln < $pfile >& ${pname}.blind.log
    #${FD} --search 'astar(hmax())' --internal-plan-file ${pname}.naive_hmax.soln < $pfile >& ${pname}.naive_hmax.log
    #${FD} --search 'astar(hmax_with_axioms(tvr=true))' --internal-plan-file ${pname}.tv_hmax.soln < $pfile >& ${pname}.tv_hmax.log
    #${FD} --search 'astar(hmax_with_axioms(tvr=false))' --internal-plan-file ${pname}.asp_hmax.soln < $pfile >& ${pname}.asp_hmax.log
    ${FD} --search 'astar(cpdbs(with_axioms=true))' --internal-plan-file ${pname}.cpdb.soln < $pfile >& ${pname}.cpdb.log
}

for file in mc-{12,14,16,18,20}-*.pre; do
    run_all_configs $file
done
