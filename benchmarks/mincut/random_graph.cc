
#include <stdlib.h>
#include "graph.h"
#include "rng.h"

void make_random_graph
(HSPS::index_type n, HSPS::weighted_graph& g, HSPS::RNG& rng)
{
  std::cerr << "creating random graph..." << std::endl;
  g.init(n);
  for (HSPS::index_type i = 0; (i + 1) < g.size(); i++)
    g.add_undirected_edge(i, i + 1);
  g.add_undirected_edge(g.size() - 1, 0);
  g.randomize_undirected_connected(n, rng);
  g.set_edge_weight(1);
}

bool check_graph
(HSPS::weighted_graph& g, int& k,
 int min_cut_size, int max_cut_size, int max_solutions)
{
  HSPS::pair_set cut;
  NTYPE f = g.min_cut(0, g.size() / 2, cut);
  k = FLOOR_TO_INT(f);
  std::cerr << "cut size = " << k << std::endl;
  assert(cut.size() == k);
  if (k < min_cut_size) return false;
  if (k > max_cut_size) return false;
  // HSPS::index_set s_nodes;
  // HSPS::index_set t_nodes;
  // for (HSPS::index_type i = 0; i < k; i++) {
  //   s_nodes.insert(cut[i].first);
  //   t_nodes.insert(cut[i].second);
  // }
  // std::cerr << s_nodes.size() << " source and " << t_nodes.size()
  // 	    << " target nodes" << std::endl;
  // if ((s_nodes.size() < k) || (t_nodes.size() < k))
  //   return false;
  // if (t_nodes.contains(g.size() / 2)) {
  //   std::cerr << "target node is in the cut!" << std::endl;
  //   return false;
  // }
  int u = 1;
  bool done = false;
  while (!done && (u < max_solutions)) {
    for (HSPS::index_type i = 0; i < cut.size(); i++)
      g.increment_edge_weight(cut[i].first, cut[i].second, 1);
    cut.clear();
    f = g.min_cut(0, g.size() / 2, cut);
    std::cerr << "new cut size = " << f << std::endl;
    if (f > k)
      return true;
    // s_nodes.clear();
    // t_nodes.clear();
    // for (HSPS::index_type i = 0; i < k; i++) {
    //   s_nodes.insert(cut[i].first);
    //   t_nodes.insert(cut[i].second);
    // }
    // std::cerr << s_nodes.size() << " source and " << t_nodes.size()
    // 	      << " target nodes" << std::endl;
    // if ((s_nodes.size() < k) || (t_nodes.size() < k))
    //   return false;
    // if (t_nodes.contains(g.size() / 2)) {
    //   std::cerr << "target node is in the cut!" << std::endl;
    //   return false;
    // }
    u += 1;
  }
  std::cerr << "too many min cuts!" << std::endl;
  return false;
}

void print_problem_strips(HSPS::weighted_graph& g, int k, HSPS::RNG& rng)
{
  HSPS::pair_set cut;
  g.set_edge_weight(1);
  NTYPE f = g.min_cut(0, g.size() / 2, cut);
  while (f == k) {
    std::cout << ";; cut = " << cut << std::endl;
    for (HSPS::index_type i = 0; i < cut.size(); i++)
      g.increment_edge_weight(cut[i].first, cut[i].second, 1);
    cut.clear();
    f = g.min_cut(0, g.size() / 2, cut);
  }
  std::cout << "(define (problem min-cut-" << g.size()
	    << "-" << k << "-" << rng.seed_value() << ")" << std::endl;
  std::cout << " (:domain min-cut)" << std::endl;
  HSPS::index_set init_pos;
  rng.select_fixed_set(init_pos, k, g.size() - 1);
  assert(init_pos.size() == k);
  HSPS::pair_set edges;
  g.edges(edges);
  std::cout << " (:objects" << std::endl;
  for (HSPS::index_type i = 0; i < g.size(); i++)
    std::cout << "  n" << i << " - node" << std::endl;
  for (HSPS::index_type i = 0; i < edges.size(); i++)
    std::cout << "  e-" << edges[i].first << "-" << edges[i].second
	      << " - edge" << std::endl;
  std::cout << "  )" << std::endl;
  std::cout << " (:init" << std::endl;
  for (HSPS::index_type i = 0; i < edges.size(); i++) {
    std::cout << "  (edge-from e-" << edges[i].first << "-" << edges[i].second
	      << " n" << edges[i].first << ")" << std::endl;
    std::cout << "  (edge-to e-" << edges[i].first << "-" << edges[i].second
	      << " n" << edges[i].second << ")" << std::endl;
  }
  for (HSPS::index_type i = 0; i < init_pos.size(); i++)
    std::cout << "  (at-node n" << init_pos[i] + 1 << ")" << std::endl;
  std::cout << "  (source-node n0)" << std::endl;
  std::cout << "  )" << std::endl;
  std::cout << " (:goal (isolated n" << g.size() / 2 << "))"
	    << std::endl;
  std::cout << " )" << std::endl;
}

void print_problem(HSPS::weighted_graph& g, int k, HSPS::RNG& rng)
{
  HSPS::pair_set cut;
  g.set_edge_weight(1);
  NTYPE f = g.min_cut(0, g.size() / 2, cut);
  while (f == k) {
    std::cout << ";; cut = " << cut << std::endl;
    for (HSPS::index_type i = 0; i < cut.size(); i++)
      g.increment_edge_weight(cut[i].first, cut[i].second, 1);
    cut.clear();
    f = g.min_cut(0, g.size() / 2, cut);
  }
  std::cout << "(define (problem min-cut-" << g.size()
	    << "-" << k << "-" << rng.seed_value() << ")" << std::endl;
  std::cout << " (:domain min-cut)" << std::endl;
  HSPS::index_set init_pos;
  rng.select_fixed_set(init_pos, k, g.size() - 1);
  assert(init_pos.size() == k);
  HSPS::pair_set edges;
  g.edges(edges);
  std::cout << " (:objects" << std::endl;
  for (HSPS::index_type i = 0; i < g.size(); i++)
    std::cout << "  n" << i << " - node" << std::endl;
  for (HSPS::index_type i = 0; i < edges.size(); i++)
    std::cout << "  e-" << edges[i].first << "-" << edges[i].second
	      << " - edge" << std::endl;
  for (HSPS::index_type i = 0; i < k; i++)
    std::cout << "  b" << i << " - block" << std::endl;
  std::cout << "  )" << std::endl;
  std::cout << " (:init" << std::endl;
  for (HSPS::index_type i = 0; i < edges.size(); i++) {
    std::cout << "  (edge-from e-" << edges[i].first << "-" << edges[i].second
	      << " n" << edges[i].first << ")" << std::endl;
    std::cout << "  (edge-to e-" << edges[i].first << "-" << edges[i].second
	      << " n" << edges[i].second << ")" << std::endl;
    HSPS::index_type nt = edges[i].second;
    for (HSPS::index_type j = 0; j < g.successors(nt).size(); j++)
      std::cout << "  (adjacent e-" << edges[i].first
		<< "-" << edges[i].second << " e-" << nt
		<< "-" << g.successors(nt)[j] << ")" << std::endl;
  }
  for (HSPS::index_type i = 0; i < init_pos.size(); i++) {
    HSPS::index_type nf = init_pos[i] + 1;
    assert(!g.successors(nf).empty());
    HSPS::index_type nt =
      g.successors(nf)[rng.random_in_range(g.successors(nf).size())];
    std::cout << "  (= (at b" << i << ") e-" << nf << "-" << nt << ")"
	      << std::endl;
  }
  std::cout << "  (source-node n0)" << std::endl;
  std::cout << "  )" << std::endl;
  std::cout << " (:goal (isolated n" << g.size() / 2 << "))"
	    << std::endl;
  std::cout << " )" << std::endl;
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    std::cout << argv[0] << ": <size>" << std::endl;
  }
  HSPS::index_type n = atoi(argv[1]);
  int min_cut_size = 2;
  int max_cut_size = n / 4;
  int max_solutions = 3; // (n / 4);
  if (argc >= 5) {
    min_cut_size = atoi(argv[2]);
    max_cut_size = atoi(argv[3]);
    max_solutions = atoi(argv[4]);
  }
  assert(n > 1);
  assert(min_cut_size > 0);
  assert(min_cut_size <= max_cut_size);
  assert(max_cut_size < n);

  HSPS::LC_RNG rng;
  rng.seed_with_pid();

  HSPS::weighted_graph g;
  int k;

  //#define MAKE_TEST
#ifdef MAKE_TEST
  g.init(12);
  g.add_undirected_edge(0, 1);
  g.add_undirected_edge(0, 2);
  g.add_undirected_edge(0, 3);
  g.add_undirected_edge(1, 4);
  g.add_undirected_edge(2, 4);
  g.add_undirected_edge(2, 5);
  g.add_undirected_edge(3, 5);
  g.add_undirected_edge(4, 6);
  g.add_undirected_edge(5, 7);
  g.add_undirected_edge(6, 8);
  g.add_undirected_edge(6, 9);
  g.add_undirected_edge(7, 9);
  g.add_undirected_edge(7, 10);
  g.add_undirected_edge(8, 11);
  g.add_undirected_edge(9, 11);
  g.add_undirected_edge(10, 11);
  g.set_edge_weight(1);
  k = 2;
#else
  // generate a random graph
  make_random_graph(n, g, rng);
  bool ok = check_graph(g, k, min_cut_size, max_cut_size, max_solutions);

  while (!ok) {
    make_random_graph(n, g, rng);
    ok = check_graph(g, k, min_cut_size, max_cut_size, max_solutions);
  }
#endif

  print_problem(g, k, rng);
}
