
#FDCLEAN=/home/patrik/pkg/fd-clean/src/search/downward-release
FD=../../src/search/downward-release

ulimit -t 1800 -m 3000000 -v 3000000

$FD --search 'astar(cpdbs(patterns=[[26,21],[25,27]]))'  --internal-plan-file p01.cpbds.soln < p01.pre >& p01.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[21,22],[17,23]]))'  --internal-plan-file p02.cpbds.soln < p02.pre >& p02.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[20,14],[19,15]]))'  --internal-plan-file p03.cpbds.soln < p03.pre >& p03.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[34,28],[32,30],[33,29]]))'  --internal-plan-file p04.cpbds.soln < p04.pre >& p04.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[43,45],[48,38],[49,35],[42,46],[41,47],[32,52],[34,50],[33,51]]))'  --internal-plan-file p05.cpbds.soln < p05.pre >& p05.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[14,23],[18,21],[22,15]]))'  --internal-plan-file p06.cpbds.soln < p06.pre >& p06.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[52,46],[51,47],[48,50]]))'  --internal-plan-file p07.cpbds.soln < p07.pre >& p07.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[33,35],[37,31],[32,36],[38,30]]))'  --internal-plan-file p08.cpbds.soln < p08.pre >& p08.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[36,38],[32,40],[35,39],[41,30]]))'  --internal-plan-file p09.cpbds.soln < p09.pre >& p09.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[24,35],[33,26],[25,34],[32,31]]))'  --internal-plan-file p10.cpbds.soln < p10.pre >& p10.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[67,62],[43,69],[58,68],[66,70]]))'  --internal-plan-file p11.cpbds.soln < p11.pre >& p11.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[34,42],[40,36],[41,35],[37,39]]))'  --internal-plan-file p12.cpbds.soln < p12.pre >& p12.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[27,30],[26,31],[32,25],[33,20]]))'  --internal-plan-file p13.cpbds.soln < p13.pre >& p13.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[35,31],[32,34],[36,30]]))'  --internal-plan-file p14.cpbds.soln < p14.pre >& p14.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[52,46],[51,44],[50,47],[49,45]]))'  --internal-plan-file p15.cpbds.soln < p15.pre >& p15.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[44,46],[43,47],[48,42]]))'  --internal-plan-file p16.cpbds.soln < p16.pre >& p16.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[42,38],[41,39],[43,36]]))'  --internal-plan-file p17.cpbds.soln < p17.pre >& p17.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[45,47],[49,43],[48,44],[42,50]]))'  --internal-plan-file p18.cpbds.soln < p18.pre >& p18.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[35,37],[33,39],[34,38],[32,40]]))'  --internal-plan-file p19.cpbds.soln < p19.pre >& p19.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[218,219]]))'  --internal-plan-file p20.cpbds.soln < p20.pre >& p20.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[41,49],[48,44],[51,39],[40,50],[34,53],[52,37],[28,55],[54,31],[57,23],[56,26],[58,20]]))'  --internal-plan-file p21.cpbds.soln < p21.pre >& p21.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[41,43],[37,46],[40,44],[45,39]]))'  --internal-plan-file p22.cpbds.soln < p22.pre >& p22.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[28,30],[27,31],[18,34],[24,32],[33,19]]))'  --internal-plan-file p23.cpbds.soln < p23.pre >& p23.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[41,36],[40,39],[33,43],[42,35],[32,44]]))'  --internal-plan-file p24.cpbds.soln < p24.pre >& p24.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[32,42],[41,35],[43,29],[40,36],[37,39]]))'  --internal-plan-file p25.cpbds.soln < p25.pre >& p25.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[42,36],[43,35],[32,44],[40,38],[41,37]]))'  --internal-plan-file p26.cpbds.soln < p26.pre >& p26.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[47,31],[48,29],[49,28],[42,43],[44,38],[36,45],[33,46]]))'  --internal-plan-file p27.cpbds.soln < p27.pre >& p27.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[38,47],[48,43],[51,37],[49,45],[50,44]]))'  --internal-plan-file p28.cpbds.soln < p28.pre >& p28.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[42,68],[41,69],[50,66],[67,43],[32,72],[34,70],[53,63],[33,71],[62,54],[64,52],[65,51],[61,55]]))'  --internal-plan-file p29.cpbds.soln < p29.pre >& p29.cpdbs.log
$FD --search 'astar(cpdbs(patterns=[[67,38],[35,68],[32,69],[43,63],[64,41],[40,65],[66,39],[61,47],[44,62],[50,60],[57,58],[59,51]]))'  --internal-plan-file p30.cpbds.soln < p30.pre >& p30.cpdbs.log
