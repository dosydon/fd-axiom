
#include "asp_check.h"
#include "globals.h"

// Add the libclasp directory to the list of 
// include directoies of your build system.
#include <clasp/program_builder.h>   // for defining logic programs
#include <clasp/unfounded_check.h>   // unfounded set checkers
#include <clasp/model_enumerators.h> // for enumerating answer sets
#include <clasp/solve_algorithms.h>  // for enumerating answer sets

//#define TRACE_PRINT_API_CALLS
//#define TRACE_PRINT_MODEL

struct ASPModelCounter : public Clasp::Enumerator::Report {
  const std::vector<int>& first_fact;
  int n_models_found;
  ASPModelCounter(const std::vector<int>& ff)
    : first_fact(ff), n_models_found(0) { };
  void reportModel(const Clasp::Solver& s, const Clasp::Enumerator&);
  void reportSolution(const Clasp::Solver& s, const Clasp::Enumerator&, bool);
  const char* value_string(Clasp::ValueRep val) {
    if (val == Clasp::value_true)
      return "T";
    else if (val == Clasp::value_false)
      return "F";
    else
      return "U";
  };
};

void ASPModelCounter::reportModel
(const Clasp::Solver& s, const Clasp::Enumerator& e) {
  // std::cout << "Model " << e.enumerated << ":" << std::endl;
  // int n_vars = (int)g_variable_domain.size();
  // int n_facts = first_fact[n_vars];
  // int c_false = n_facts + 1;
  // for (unsigned int i = 0; i < g_variable_domain.size(); i++) {
  //   if (g_axiom_layers[i] < 0) {
  //     for (unsigned int j = 0; j < g_variable_domain[i]; j++)
  // 	std::cout << g_variable_name[i] << "=" << g_fact_names[i][j]
  // 		  << "(" << first_fact[i] + j << "): "
  // 		  << value_string(s.value(first_fact[i] + j))
  // 		  << std::endl;
  //   }
  //   else {
  //     std::cout << "(derived) " << g_variable_name[i]
  // 		<< "(" << first_fact[i] << "): "
  // 		<< value_string(s.value(first_fact[i]))
  // 		<< std::endl;
  //   }
  // }
  // std::cout << "c_false (" << c_false << "): "
  // 	    << value_string(s.value(c_false)) << std::endl;
  n_models_found += 1;
}

void ASPModelCounter::reportSolution
(const Clasp::Solver&, const Clasp::Enumerator&, bool complete)
{
  // ignored
}

ASPChecker::ASPChecker()
  : n_calls(0)
{
  asp_time_total.stop();
  asp_time_total.reset();
  asp_time_solve.stop();
  asp_time_solve.reset();
  assert(g_variable_domain.size() == g_axiom_layers.size());
  first_fact.resize(g_variable_domain.size() + 1, 1);
  // build fact index:
  for (unsigned int i = 0; i < g_axiom_layers.size(); i++) {
    if (g_axiom_layers[i] >= 0) {
      // this is a secondary (derived) variable: check that it
      // satisfies assumptions.
      assert(g_variable_domain[i] == 2);
      //assert(g_default_axiom_values[i] == 0);
      first_fact[i + 1] = first_fact[i] + 1;
    }
    else {
      // this is a primary variable
      first_fact[i + 1] = first_fact[i] + g_variable_domain[i];
    }
    //std::cout << "first_fact[" << i << "] = " << first_fact[i] << std::endl;
  }
  // if we implement some kind of incremental construction of the
  // ASP, more initialisations may be needed.
}

ASPChecker::~ASPChecker()
{
  // nothing to destroy!
}

static void asp_define_variables
(const std::vector<int>& first_fact,
 Clasp::ProgramBuilder& api)
{
  int n_vars = (int)g_variable_domain.size();
  // ASP (boolean) vars will be numbered 1..n_facts:
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;
  api.setCompute(c_false, false);
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "api.setCompute(" << c_false << ", false);" << std::endl;
#endif
  // for each primary variable...
  for (int var = 0; var < n_vars; var++)
    if (g_axiom_layers[var] < 0) {
      // create a "ch oice rule" for each value of the variable:
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "// primary variable " << g_variable_name[var] << std::endl;
#endif
      api.startRule(Clasp::CHOICERULE);
      for (int val = 0; val < g_variable_domain[var]; val++)
	api.addHead(first_fact[var] + val);
      api.endRule();
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.startRule(Clasp::CHOICERULE);" << std::endl;
      for (int val = 0; val < g_variable_domain[var]; val++)
	std::cout << "api.addHead(" << first_fact[var] + val << ");"
		  << std::endl;
      std::cout << "api.endRule();" << std::endl;
#endif
      // create an integrity constraint for each mutex pair of values:
      for (int i = 0; i < g_variable_domain[var]; i++)
	for (int j = i + 1; j < g_variable_domain[var]; j++) {
	  api.startRule(Clasp::BASICRULE);
	  api.addHead(c_false);
	  api.addToBody(first_fact[var] + i, true);
	  api.addToBody(first_fact[var] + j, true);
	  api.endRule();
#ifdef TRACE_PRINT_API_CALLS
	  std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
	  std::cout << "api.addHead(" << c_false << ");" << std::endl;
	  std::cout << "api.addToBody(" << first_fact[var] + i << ", true);"
		    << std::endl;
	  std::cout << "api.addToBody(" << first_fact[var] + j << ", true);"
		    << std::endl;
	  std::cout << "api.endRule();" << std::endl;
#endif
	}
      // create an integrity constraint for at-least-1 value:
      // (FALSE :- not p, not q) <=> (p | q).
      api.startRule(Clasp::BASICRULE);
      api.addHead(c_false);
      for (int val = 0; val < g_variable_domain[var]; val++)
	api.addToBody(first_fact[var] + val, false);
      api.endRule();
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
      std::cout << "api.addHead(" << c_false << ");" << std::endl;
      for (int val = 0; val < g_variable_domain[var]; val++)
	std::cout << "api.addToBody(" << first_fact[var] + val << ", false);"
		  << std::endl;
      std::cout << "api.endRule();" << std::endl;
#endif
    }
}

static void asp_define_axioms
(const std::vector<int>& first_fact,
 Clasp::ProgramBuilder& api)
{
  int n_vars = (int)g_variable_domain.size();
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;
  // for each axiom...
  for (unsigned int k = 0; k < g_axioms.size(); k++) {
    const GlobalOperator& ax = g_axioms[k];
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "// axiom #" << k << std::endl;
    std::cout << "// ";
    g_axioms[k].dump();
#endif
    // should be an axiom and have a single effect that sets a derived
    // variable to true; the precondition of the effect is only that
    // the variable is false, and the actual rule body is in the effect
    // condition.
    assert(ax.is_axiom());
    assert(ax.get_effects().size() == 1);
    const GlobalEffect& rule = ax.get_effects()[0];
    int rvar = rule.var;
    assert(g_axiom_layers[rvar] >= 0);
    assert(rule.val != g_default_axiom_values[rvar]);
    assert(ax.get_preconditions().size() == 1);
    assert(ax.get_preconditions()[0].var == rvar);
    assert(ax.get_preconditions()[0].val == g_default_axiom_values[rvar]);
    // make a rule:
    api.startRule(Clasp::BASICRULE);
    api.addHead(first_fact[rvar]);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
    std::cout << "api.addHead(" << first_fact[ax.get_effects()[0].var] << ");"
	      << std::endl;
#endif
    const std::vector<GlobalCondition>& body = rule.conditions;
    for (unsigned int j = 0; j < body.size(); j++) {
      int cvar = body[j].var;
      int cval = body[j].val;
      if (g_axiom_layers[cvar] < 0) {
	assert(cval < g_variable_domain[cvar]);
	api.addToBody(first_fact[cvar] + cval, true);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] + cval << ", true);"
		  << std::endl;
#endif
      }
      else if (cval != g_default_axiom_values[cvar]) {
	api.addToBody(first_fact[cvar], true);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
		  << std::endl;
#endif
      }
      else {
	assert(cval == g_default_axiom_values[cvar]);
	api.addToBody(first_fact[cvar], false);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
		  << std::endl;
#endif
      }
    }
    api.endRule();
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.endRule();" << std::endl;
#endif
  }
}

static void asp_define_requirements
(const std::vector<int>& first_fact,
 Clasp::ProgramBuilder& api,
 const std::vector<GlobalCondition> & reqs)
{
  int n_vars = (int)g_variable_domain.size();
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;
  // for each required condition, add it's negation as an
  // integrity constraint:
  for (unsigned int i = 0; i < reqs.size(); i++) {
    int cvar = reqs[i].var;
    int cval = reqs[i].val;
    // assert(g_axiom_layers[cvar] >= 0);
    assert(cval < g_variable_domain[cvar]);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "// require " << g_variable_name[cvar] << std::endl;
#endif
    api.startRule(Clasp::BASICRULE);
    api.addHead(c_false);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
    std::cout << "api.addHead(" << c_false << ");" << std::endl;
    std::cout << "api.endRule();" << std::endl;
#endif
    // requirement on a secondary (derived) variable:
    if (g_axiom_layers[cvar] >= 0) {
      if (cval != g_default_axiom_values[cvar]) {
	api.addToBody(first_fact[cvar], false);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
		  << std::endl;
#endif
      }
      else {
	assert(cval == g_default_axiom_values[cvar]);
	api.addToBody(first_fact[cvar], true);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
		  << std::endl;
#endif
      }
    }
    // requirement on a primary variable:
    else {
      api.addToBody(first_fact[cvar] + cval, false);
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.addToBody(" << first_fact[cvar] + cval << ", false);"
		<< std::endl;
#endif
    }
    api.endRule();
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.endRule();" << std::endl;
#endif
  }
}

static bool asp_check
(const std::vector<int>& first_fact,
 Clasp::ProgramBuilder& api,
 Clasp::SharedContext& ctx,
 Timer& asp_time_solve)
{
  bool ok = api.endProgram();
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "api.endProgram();" << std::endl;
#endif

  // api.writeProgram(std::cout);

  // endProgram returns false if the program is found to be inconsistent
  // in preprocessing; it seems that calling solve in this case causes
  // a segfault.
  if (!ok) {
    return false;
  }

  // stuff that has to be there...
  if (api.dependencyGraph() && api.dependencyGraph()->nodes() > 0) {
    Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();
    ufs->attachTo(*ctx.master(), api.dependencyGraph());
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();" << std::endl;
    std::cout << "ufs->attachTo(*ctx.master(), api.dependencyGraph());"
	      << std::endl;
#endif
  }

  ASPModelCounter counter(first_fact);
  ctx.addEnumerator(new Clasp::BacktrackEnumerator(0, &counter));
  ctx.enumerator()->enumerate(1);

  ctx.endInit();
  Clasp::SolveParams sparams;
  asp_time_solve.resume();
  Clasp::solve(ctx, sparams);
  asp_time_solve.stop();
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "ctx.endInit();" << std::endl;
  std::cout << "Clasp::SolveParams sparams;" << std::endl;
  std::cout << "bool ok = Clasp::solve(ctx, sparams);" << std::endl;
#endif

  // all deallocs are (I presume) automatic, so we can just return...
  return (counter.n_models_found > 0);
}

// check if requirements are satisfiable in a projection.
// - pattern is the set of projected-on variables, whose values
//   are taken from the state; pattern variables can only be
//   primary (non-derived).
// - reqs is the list (conjunction) of required conditions on
//   derived variables.
bool ASPChecker::check_projection
(const std::vector<int> & pattern,
 const std::vector<int> & state,
 const std::vector<GlobalCondition> & reqs)
{
  n_calls += 1;
  asp_time_total.resume();
  Clasp::ProgramBuilder api;
  Clasp::SharedContext ctx;
  api.startProgram(ctx);
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "Clasp::ProgramBuilder api;" << std::endl;
  std::cout << "Clasp::SharedContext ctx;" << std::endl;
  std::cout << "api.startProgram(ctx);" << std::endl;
#endif

  asp_define_variables(first_fact, api);
  asp_define_axioms(first_fact, api);

  // add an integrity constraint to force the value of each variable
  // in the pattern:
  int n_vars = (int)g_variable_domain.size();
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;
  for (unsigned int i = 0; i < pattern.size(); i++) {
    assert(pattern[i] < n_vars);
    int pvar = pattern[i];
    assert(g_axiom_layers[pvar] < 0);
    //int pval = state[pvar];
    int pval = state[i];
    assert(pval < g_variable_domain[pvar]);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "// " << g_variable_name[pvar] << " = " << pval << std::endl;
#endif
    api.startRule(Clasp::BASICRULE);
    api.addHead(c_false);
    api.addToBody(first_fact[pvar] + pval, false);
    api.endRule();
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
    std::cout << "api.addHead(" << c_false << ");" << std::endl;
    std::cout << "api.addToBody(" << first_fact[pvar] + pval << ", false);"
	      << std::endl;
    std::cout << "api.endRule();" << std::endl;
#endif
  }

  asp_define_requirements(first_fact, api, reqs);

  // ASP complete
  bool ok = asp_check(first_fact, api, ctx, asp_time_solve);
  asp_time_total.stop();

  // all deallocs are (I presume) automatic, so we can just return...
  return ok;
}

bool ASPChecker::check_relaxed_state
(const std::vector< std::vector<bool> > & rstate,
 const std::vector<GlobalCondition> & reqs)
{
  n_calls += 1;
  asp_time_total.resume();
  Clasp::ProgramBuilder api;
  Clasp::SharedContext ctx;
  api.startProgram(ctx);
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "Clasp::ProgramBuilder api;" << std::endl;
  std::cout << "Clasp::SharedContext ctx;" << std::endl;
  std::cout << "api.startProgram(ctx);" << std::endl;
#endif

  asp_define_variables(first_fact, api);
  asp_define_axioms(first_fact, api);

  // add constraints to exlude values not possible in the relaxed
  // state: not implemented!!
  int n_vars = (int)g_variable_domain.size();
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;
  for (unsigned int i = 0; i < g_variable_domain.size(); i++)
    if (g_axiom_layers[i] < 0) {
      unsigned int pvar = i;
      for (unsigned int pval = 0; pval < g_variable_domain[pvar]; pval++)
	if (!rstate[pvar][pval]) {
#ifdef TRACE_PRINT_API_CALLS
	  std::cout << "// " << g_variable_name[pvar] << " =/= " << pval
		    << std::endl;
#endif
	  api.startRule(Clasp::BASICRULE);
	  api.addHead(c_false);
	  api.addToBody(first_fact[pvar] + pval, true);
	  api.endRule();
#ifdef TRACE_PRINT_API_CALLS
	  std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
	  std::cout << "api.addHead(" << c_false << ");" << std::endl;
	  std::cout << "api.addToBody(" << first_fact[pvar] + pval
		    << ", true);"
		    << std::endl;
	  std::cout << "api.endRule();" << std::endl;
#endif
	}
    }

  asp_define_requirements(first_fact, api, reqs);

  // ASP complete
  bool ok = asp_check(first_fact, api, ctx, asp_time_solve);
  asp_time_total.stop();

  // all deallocs are (I presume) automatic, so we can just return...
  return ok;
}


////
// OLD CHECK_PROJECTION
////

// bool ASPChecker::check_projection
// (const std::vector<int> & pattern,
//  const std::vector<int> & state,
//  const std::vector<GlobalCondition> & reqs)
// {
//   n_calls += 1;
//   Clasp::ProgramBuilder api;
//   Clasp::SharedContext ctx;
//   api.startProgram(ctx);
// #ifdef TRACE_PRINT_API_CALLS
//   std::cout << "Clasp::ProgramBuilder api;" << std::endl;
//   std::cout << "Clasp::SharedContext ctx;" << std::endl;
//   std::cout << "api.startProgram(ctx);" << std::endl;
// #endif
// 
//   int n_vars = (int)g_variable_domain.size();
//   // ASP (boolean) vars will be numbered 1..n_facts:
//   int n_facts = first_fact[n_vars];
//   int c_false = n_facts + 1;
//   api.setCompute(c_false, false);
// #ifdef TRACE_PRINT_API_CALLS
//   std::cout << "api.setCompute(" << c_false << ", false);" << std::endl;
// #endif
// 
//   // for each primary variable...
//   for (int var = 0; var < n_vars; var++)
//     if (g_axiom_layers[var] < 0) {
//       // create a "choice rule" for each value of the variable:
// #ifdef TRACE_PRINT_API_CALLS
//       std::cout << "// primary variable " << g_variable_name[var] << std::endl;
// #endif
//       api.startRule(Clasp::CHOICERULE);
//       for (int val = 0; val < g_variable_domain[var]; val++)
// 	api.addHead(first_fact[var] + val);
//       api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
//       std::cout << "api.startRule(Clasp::CHOICERULE);" << std::endl;
//       for (int val = 0; val < g_variable_domain[var]; val++)
// 	std::cout << "api.addHead(" << first_fact[var] + val << ");"
// 		  << std::endl;
//       std::cout << "api.endRule();" << std::endl;
// #endif
//       // create an integrity constraint for each mutex pair of values:
//       for (int i = 0; i < g_variable_domain[var]; i++)
// 	for (int j = i + 1; j < g_variable_domain[var]; j++) {
// 	  api.startRule(Clasp::BASICRULE);
// 	  api.addHead(c_false);
// 	  api.addToBody(first_fact[var] + i, true);
// 	  api.addToBody(first_fact[var] + j, true);
// 	  api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
// 	  std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
// 	  std::cout << "api.addHead(" << c_false << ");" << std::endl;
// 	  std::cout << "api.addToBody(" << first_fact[var] + i << ", true);"
// 		    << std::endl;
// 	  std::cout << "api.addToBody(" << first_fact[var] + j << ", true);"
// 		    << std::endl;
// 	  std::cout << "api.endRule();" << std::endl;
// #endif
// 	}
//       // create an integrity constraint for at-least-1 value:
//       // (FALSE :- not p, not q) <=> (p | q).
//       api.startRule(Clasp::BASICRULE);
//       api.addHead(c_false);
//       for (int val = 0; val < g_variable_domain[var]; val++)
// 	api.addToBody(first_fact[var] + val, false);
//       api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
//       std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
//       std::cout << "api.addHead(" << c_false << ");" << std::endl;
//       for (int val = 0; val < g_variable_domain[var]; val++)
// 	std::cout << "api.addToBody(" << first_fact[var] + val << ", false);"
// 		  << std::endl;
//       std::cout << "api.endRule();" << std::endl;
// #endif
//     }
// 
//   // for each axiom...
//   for (unsigned int k = 0; k < g_axioms.size(); k++) {
//     const GlobalOperator& ax = g_axioms[k];
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "// axiom #" << k << std::endl;
//     std::cout << "// ";
//     g_axioms[k].dump();
// #endif
//     // should be an axiom and have a single effect that sets a derived
//     // variable to true; the precondition of the effect is only that
//     // the variable is false, and the actual rule body is in the effect
//     // condition.
//     assert(ax.is_axiom());
//     assert(ax.get_effects().size() == 1);
//     const GlobalEffect& rule = ax.get_effects()[0];
//     int rvar = rule.var;
//     assert(g_axiom_layers[rvar] >= 0);
//     assert(rule.val != g_default_axiom_values[rvar]);
//     assert(ax.get_preconditions().size() == 1);
//     assert(ax.get_preconditions()[0].var == rvar);
//     assert(ax.get_preconditions()[0].val == g_default_axiom_values[rvar]);
//     // make a rule:
//     api.startRule(Clasp::BASICRULE);
//     api.addHead(first_fact[rvar]);
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
//     std::cout << "api.addHead(" << first_fact[ax.get_effects()[0].var] << ");"
// 	      << std::endl;
// #endif
//     const std::vector<GlobalCondition>& body = rule.conditions;
//     for (unsigned int j = 0; j < body.size(); j++) {
//       int cvar = body[j].var;
//       int cval = body[j].val;
//       if (g_axiom_layers[cvar] < 0) {
// 	assert(cval < g_variable_domain[cvar]);
// 	api.addToBody(first_fact[cvar] + cval, true);
// #ifdef TRACE_PRINT_API_CALLS
// 	std::cout << "api.addToBody(" << first_fact[cvar] + cval << ", true);"
// 		  << std::endl;
// #endif
//       }
//       else if (cval != g_default_axiom_values[cvar]) {
// 	api.addToBody(first_fact[cvar], true);
// #ifdef TRACE_PRINT_API_CALLS
// 	std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
// 		  << std::endl;
// #endif
//       }
//       else {
// 	assert(cval == g_default_axiom_values[cvar]);
// 	api.addToBody(first_fact[cvar], false);
// #ifdef TRACE_PRINT_API_CALLS
// 	std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
// 		  << std::endl;
// #endif
//       }
//     }
//     api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "api.endRule();" << std::endl;
// #endif
//   }
// 
//   // add an integrity constraint to force the value of each variable
//   // in the pattern:
//   for (unsigned int i = 0; i < pattern.size(); i++) {
//     assert(pattern[i] < n_vars);
//     int pvar = pattern[i];
//     assert(g_axiom_layers[pvar] < 0);
//     //int pval = state[pvar];
//     int pval = state[i];
//     assert(pval < g_variable_domain[pvar]);
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "// " << g_variable_name[pvar] << " = " << pval << std::endl;
// #endif
//     api.startRule(Clasp::BASICRULE);
//     api.addHead(c_false);
//     api.addToBody(first_fact[pvar] + pval, false);
//     api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
//     std::cout << "api.addHead(" << c_false << ");" << std::endl;
//     std::cout << "api.addToBody(" << first_fact[pvar] + pval << ", false);"
// 	      << std::endl;
//     std::cout << "api.endRule();" << std::endl;
// #endif
//   }
// 
//   // for each required condition, add it's negation as an integrity constraint:
//   for (unsigned int i = 0; i < reqs.size(); i++) {
//     int cvar = reqs[i].var;
//     int cval = reqs[i].val;
//     assert(g_axiom_layers[cvar] >= 0);
//     assert(cval < g_variable_domain[cvar]);
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "// require " << g_variable_name[cvar] << std::endl;
// #endif
//     api.startRule(Clasp::BASICRULE);
//     api.addHead(c_false);
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
//     std::cout << "api.addHead(" << c_false << ");" << std::endl;
//     std::cout << "api.endRule();" << std::endl;
// #endif
//     if (cval != g_default_axiom_values[cvar]) {
//       api.addToBody(first_fact[cvar], false);
// #ifdef TRACE_PRINT_API_CALLS
//       std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
// 		<< std::endl;
// #endif
//     }
//     else {
//       assert(cval == g_default_axiom_values[cvar]);
//       api.addToBody(first_fact[cvar], true);
// #ifdef TRACE_PRINT_API_CALLS
//       std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
// 		<< std::endl;
// #endif
//     }
//     api.endRule();
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "api.endRule();" << std::endl;
// #endif
//   }
// 
//   // ASP complete
//   bool ok = api.endProgram();
// #ifdef TRACE_PRINT_API_CALLS
//   std::cout << "api.endProgram();" << std::endl;
// #endif
// 
//   // api.writeProgram(std::cout);
// 
//   // endProgram returns false if the program is found to be inconsistent
//   // in preprocessing; it seems that calling solve in this case causes
//   // a segfault.
//   if (!ok) {
//     //std::cout << "inconsistent in preprocessing" << std::endl;
//     return false;
//   }
// 
//   // stuff that has to be there...
//   if (api.dependencyGraph() && api.dependencyGraph()->nodes() > 0) {
//     std::cout << "adding unfounded check" << std::endl;
//     Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();
//     ufs->attachTo(*ctx.master(), api.dependencyGraph());
// #ifdef TRACE_PRINT_API_CALLS
//     std::cout << "Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();" << std::endl;
//     std::cout << "ufs->attachTo(*ctx.master(), api.dependencyGraph());"
// 	      << std::endl;
// #endif
//   }
// 
//   ASPModelCounter counter(first_fact);
//   ctx.addEnumerator(new Clasp::BacktrackEnumerator(0, &counter));
//   ctx.enumerator()->enumerate(1);
// 
//   ctx.endInit();
//   Clasp::SolveParams sparams;
//   Clasp::solve(ctx, sparams);
// #ifdef TRACE_PRINT_API_CALLS
//   std::cout << "ctx.endInit();" << std::endl;
//   std::cout << "Clasp::SolveParams sparams;" << std::endl;
//   std::cout << "bool ok = Clasp::solve(ctx, sparams);" << std::endl;
// #endif
// 
//   // all deallocs are (I presume) automatic, so we can just return...
//   return (counter.n_models_found > 0);
// }
