
#include "hmax_with_axioms.h"
#include "asp_check.h"
//#include "global_operator.h"
#include "global_state.h"
//#include "priority_queue.h"
#include "option_parser.h"
#include "plugin.h"

// #define TRACE_PRINT_LOTS

#include <cassert>
#include <vector>
using namespace std;

#ifdef TRACE_PRINT_LOTS
static void print_relaxed_state
(const std::vector< std::vector<bool> >& rstate)
{
  for (unsigned int i = 0; i < g_variable_domain.size(); i++)
    if (g_axiom_layers[i] < 0) {
      cout << " " << g_variable_name[i] << ":";
      for (unsigned int j = 0; j < g_variable_domain[i]; j++)
	if (rstate[i][j]) {
	  cout << " " << g_fact_names[i][j];
	}
      cout << endl;
    }
}

static void print_conditions
(const std::vector<GlobalCondition>& conditions)
{
  bool first = true;
  for (size_t i = 0; i < conditions.size(); i++) {
    int var = conditions[i].var;
    int val = conditions[i].val;
    if (!first)
      cout << " & ";
    cout << g_variable_name[var] << "=" << g_fact_names[var][val];
    first = false;
  }
}
#endif

// construction and destruction
HMaxWithAxioms::HMaxWithAxioms(const Options &opts)
  : Heuristic(opts), checker(0)
{
  //verify_no_conditional_effects();
  use_tvr = opts.get<bool>("tvr");
  n_calls = 0;
}

HMaxWithAxioms::~HMaxWithAxioms()
{
}

void HMaxWithAxioms::find_axioms_in_layer
(int layer, std::vector<GlobalEffect>& axioms_in_layer)
{
  for (unsigned int i = 0; i < g_axioms.size(); i++) {
    const std::vector<GlobalEffect>& effs = g_axioms[i].get_effects();
    assert(effs.size() == 1);
    if (g_axiom_layers[effs[0].var] == layer)
      axioms_in_layer.push_back(effs[0]);
  }
}

// initialization
void HMaxWithAxioms::initialize()
{
  cout << "Initializing h^max with axioms..." << endl;
  // make sure memory is allocated for rstate and remaining_effects:
  rstate.resize(g_variable_domain.size());
  for (unsigned int i = 0; i < g_variable_domain.size(); i++) {
    rstate[i].resize(g_variable_domain[i], false);
  }
  remaining_effects.resize(g_operators.size());
  for (unsigned int i = 0; i < g_operators.size(); i++) {
    remaining_effects[i].resize(g_operators[i].get_effects().size(), true);
  }
  // if we are going to use 3-valued relaxation, init axiom layer
  // structure (to be used for relaxed evaluation):
  if (use_tvr) {
    int layer_no = 0;
    std::vector<GlobalEffect> layer;
    find_axioms_in_layer(layer_no, layer);
    while (layer.size() > 0) {
      layers.push_back(layer);
      layer_no += 1;
      layer.clear();
      find_axioms_in_layer(layer_no, layer);
    }
  }
  // if not, create an ASP checker:
  else {
    checker = new ASPChecker();
  }
  // rebuild goal as GlobalCondition vector:
  for (unsigned int i = 0; i < g_goal.size(); i++) {
    int var = g_goal[i].first;
    int val = g_goal[i].second;
    goal_condition.push_back(GlobalCondition(var, val));
  }
}

bool HMaxWithAxioms::evaluate
(const std::vector<GlobalCondition>& condition,
 const std::vector< std::vector<bool> >& rstate)
{
  if (use_tvr) {
    for (unsigned int i = 0; i < condition.size(); i++)
      if (!rstate[condition[i].var][condition[i].val])
	return false;
    return true;
  }
  else {
    //std::vector<GlobalCondition> secondary;
    bool has_secondary = false;
    for (unsigned int i = 0; i < condition.size(); i++)
      if (g_axiom_layers[condition[i].var] < 0) {
	if (!rstate[condition[i].var][condition[i].val])
	  return false;
      }
      else {
	has_secondary = true;
	// secondary.push_back(condition[i]);
      }
    if (has_secondary) {
      // if (secondary.size() > 0) {
      // cout << "checking ";
      // print_conditions(secondary);
      // cout << endl;
      // return checker->check_relaxed_state(rstate, secondary);
      return checker->check_relaxed_state(rstate, condition);
    }
    return true;
  }
}

// returns 0 = false, 1 = may be true, 2 = is definitely true
int HMaxWithAxioms::evaluate3
(const std::vector<GlobalCondition>& condition,
 const std::vector< std::vector<bool> >& rstate)
{
  int status = 2;
  for (unsigned int i = 0; i < condition.size(); i++) {
    if (!rstate[condition[i].var][condition[i].val])
      return 0;
    if (status > 1) {
      int var = condition[i].var;
      for (int j = 0; (j < g_variable_domain[var]) && (status > 1); j++)
	if ((j != condition[i].val) && rstate[var][j])
	  status = 1;
    }
  }
  return status;
}

void HMaxWithAxioms::enqueue_effects
(int current_cost,
 const std::vector< std::vector<bool> >& rstate,
 std::vector< std::vector<bool> >& remaining_effects,
 BucketQueue<GlobalCondition>& queue)
{
  for (unsigned int k = 0; k < g_operators.size(); k++) {
    assert(!g_operators[k].is_axiom());
    vector<bool>::iterator p =
      find(remaining_effects[k].begin(), remaining_effects[k].end(), true);
    if (p != remaining_effects[k].end()) {
#ifdef TRACE_PRINT_LOTS
      cout << "checking pre(" << g_operators[k].get_name() << ") = ";
      print_conditions(g_operators[k].get_preconditions());
      cout << endl;
#endif
      bool is_app = evaluate(g_operators[k].get_preconditions(), rstate);
      if (is_app) {
	const std::vector<GlobalEffect>& effs = g_operators[k].get_effects();
#ifdef TRACE_PRINT_LOTS
	cout << "action " << g_operators[k].get_name()
	     << " applicable (" << effs.size() << " effects)" << endl;
#endif
	int eff_cost = current_cost + get_adjusted_cost(g_operators[k]);
	for (unsigned int i = 0; i < effs.size(); i++)
	  if (remaining_effects[k][i]) {
	    int evar = effs[i].var;
	    int eval = effs[i].val;
	    assert(g_axiom_layers[evar] < 0);
	    if (!rstate[evar][eval]) {
#ifdef TRACE_PRINT_LOTS
	      cout << "checking condition: ";
	      print_conditions(effs[i].conditions);
	      cout << " of effect " << i << ": " << g_variable_name[evar]
		   << ":=" << g_fact_names[evar][eval] << endl;
#endif
	      if (evaluate(effs[i].conditions, rstate)) {
#ifdef TRACE_PRINT_LOTS
		cout << "condition holds, enqueueing effect..." << endl;
#endif
		queue.push(eff_cost, GlobalCondition(evar, eval));
		remaining_effects[k][i] = false;
	      }
	    }
	    else {
#ifdef TRACE_PRINT_LOTS
	      cout << "effect " << i << ": " << g_variable_name[evar]
		   << ":=" << g_fact_names[evar][eval]
		   << " is already achieved" << endl;
	      remaining_effects[k][i] = false;
#endif
	    }
	  }
#ifdef TRACE_PRINT_LOTS
	  else {
	    cout << "effect " << i << " is non-remaining" << endl;
	  }
#endif
      }
    }
  }
}

void HMaxWithAxioms::apply_axioms
(const std::vector<GlobalEffect> axioms,
 std::vector< std::vector<bool> >& rstate)
{
  // standard, simple fix-point calculation
  bool done = false;
  std::vector<bool> relevant(axioms.size(), true);
  while (!done) {
    done = true;
    // evaluate each still-relevant axiom
    for (unsigned int i = 0; i < axioms.size(); i++)
      if (relevant[i]) {
	int v = evaluate3(axioms[i].conditions, rstate);
	// if axiom condition may be true...
	if (v > 0) {
	  // ...flag value as possible in relaxed state
	  if (!rstate[axioms[i].var][axioms[i].val]) {
	    rstate[axioms[i].var][axioms[i].val] = true;
	    done = false;
	  }
	  // if axiom condition must be true...
	  if (v == 2) {
	    int dval = g_default_axiom_values[axioms[i].var];
	    assert(axioms[i].val != dval);
	    // ...flag alternate (default) value as NOT possible
	    if (rstate[axioms[i].var][dval]) {
	      rstate[axioms[i].var][dval] = false;
	      done = false;
	      // at this point, all axioms that assign the same
	      // variable become redundant.
	      for (unsigned int j = 0; j < axioms.size(); j++)
		if (axioms[j].var == axioms[i].var)
		  relevant[j] = false;
	    }
	  }
	}
      }
  }
}

void HMaxWithAxioms::update_derived_variables()
{
  if (!use_tvr) return;
  // reset all derived variables to default values only
  for (unsigned int i = 0; i < g_variable_domain.size(); i++)
    if (g_axiom_layers[i] >= 0)
      for (unsigned int j = 0; j < (size_t)g_variable_domain[i]; j++)
	rstate[i][j] = ((int)j == g_default_axiom_values[i]);
  for (unsigned int l = 0; l < layers.size(); l++) {
    // cout << "applying " << layers[l].size()
    // 	 << " axioms in layer " << l << endl;
    apply_axioms(layers[l], rstate);
  }
}

int HMaxWithAxioms::compute_heuristic(const GlobalState &state)
{
  // if (!use_tvr)
  //   if ((n_calls % 100) == 99) {
  //     cout << checker->number_of_calls() << " ASP calls in "
  // 	   << n_calls << " h^max evaluations" << endl;
  //   }
  n_calls += 1;

  // initialise rstate to current actual state:
  for (unsigned int i = 0; i < g_variable_domain.size(); i++)
    for (unsigned int j = 0; j < (size_t)g_variable_domain[i]; j++)
      rstate[i][j] = (state[i] == (int)j);
  for (unsigned int k = 0; k < g_operators.size(); k++)
    for (unsigned int i = 0; i < g_operators[k].get_effects().size(); i++)
      remaining_effects[k][i] = true;
  int current_cost = 0;

#ifdef TRACE_PRINT_LOTS
  cout << "@ zero" << endl;
  print_relaxed_state(rstate);
#endif

  bool goal_is_true = evaluate(goal_condition, rstate);
  if (goal_is_true) return current_cost;
  BucketQueue<GlobalCondition> queue;
  enqueue_effects(current_cost, rstate, remaining_effects, queue);

  while (!queue.empty()) {
    int new_cost = queue.peek();
    assert(new_cost >= current_cost);
    bool done = false;
    bool changed = false;
    while (!done) {
      pair<int, GlobalCondition> next_pair = queue.pop();
      assert(next_pair.first == new_cost);
      int var = next_pair.second.var;
      int val = next_pair.second.val;
      if (!rstate[var][val]) {
	rstate[var][val] = true;
	changed = true;
      }
      if (queue.empty()) done = true;
      if (!done)
	if (queue.peek() > new_cost) done = true;
    }
    current_cost = new_cost;
    if (changed) {
      update_derived_variables();
#ifdef TRACE_PRINT_LOTS
      cout << "@ cost = " << current_cost << endl;
      print_relaxed_state(rstate);
#endif
      goal_is_true = evaluate(goal_condition, rstate);
      if (goal_is_true) return current_cost;
      enqueue_effects(current_cost, rstate, remaining_effects, queue);
    }
  }
  return DEAD_END;
}

bool HMaxWithAxioms::dead_ends_are_reliable() const
{
  return true;
}

static Heuristic *_parse(OptionParser &parser)
{
  parser.document_synopsis("Max heuristic", "");
  parser.document_language_support("action costs", "supported");
  parser.document_language_support("conditional effects", "supported");
  parser.document_language_support("axioms", "supported");
  parser.document_property("admissible", "yes");
  parser.document_property("consistent", "yes");
  parser.document_property("safe", "yes");
  parser.document_property("preferred operators", "no");

  Heuristic::add_options_to_parser(parser);
  parser.add_option<bool>("tvr", "use 3-valued relaxation", "true");
  Options opts = parser.parse();

  if (parser.dry_run())
    return 0;

  return new HMaxWithAxioms(opts);
}

static Plugin<Heuristic> _plugin("hmax_with_axioms", _parse);
