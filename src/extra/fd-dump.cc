
#include "state_registry.h"
#include "axioms.h"
#include "globals.h"
#include "global_operator.h"
#include "utilities.h"

#include <iostream>
#include <cstring>
using namespace std;

int main(int argc, const char **argv) {
  int verbosity = argc - 1;

  istream &in = cin;
  read_everything(in);

  cout << "==== VARIABLES ====" << endl;
  for (int i = 0; i < g_variable_name.size(); i++) {
    cout << i << ". " << g_variable_name[i]
	 << ": layer = " << g_axiom_layers[i]
	 << ", default = " << g_default_axiom_values[i]
	 << " " << g_fact_names[i][g_default_axiom_values[i]]
	 << ", domain size = " << g_variable_domain[i]
	 << endl;
    for (int j = 0; j < g_variable_domain[i]; j++)
      cout << "  " << j << ". " << g_fact_names[i][j] << endl;
  }

  cout << "==== OPERATORS ====" << endl;
  for (int i = 0; i < g_operators.size(); i++) {
    g_operators[i].dump();
  }

  cout << "==== AXIOMS ====" << endl;
  for (int i = 0; i < g_axioms.size(); i++) {
    // g_axioms[i].dump();
    const std::vector<GlobalCondition>& pre = g_axioms[i].get_preconditions();
    const std::vector<GlobalEffect>& eff = g_axioms[i].get_effects();
    assert(pre.size() == 1);
    assert(eff.size() == 1);
    int avar = pre[0].var;
    int afrom = pre[0].val;
    int ato = eff[0].val;
    const std::vector<GlobalCondition>& body = eff[0].conditions;
    cout << "axiom: " << avar << ". " << g_variable_name[avar]
	 << " from " << afrom << ". " << g_fact_names[avar][afrom]
	 << " to " << ato << ". " << g_fact_names[avar][ato]
	 << " if";
    for (unsigned int j = 0; j < body.size(); j++)
      cout << " " << g_fact_names[body[j].var][body[j].val];
    cout << endl;
  }

  return 0;
}
